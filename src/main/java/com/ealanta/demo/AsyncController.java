package com.ealanta.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.async.DeferredResult;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class AsyncController {

	private static AtomicInteger COUNTER = new AtomicInteger(0);

	private final ObjectMapper mapper = new ObjectMapper();

	@RequestMapping("/async")
	public DeferredResult<ResponseEntity<String>> getAsyncResponse(
			@RequestParam("delayMs") long delayMs,
			@RequestParam("requestId") int requestId,
			@RequestParam("userId") int userId
			) {
		CompletableFuture<Void> result1 = CompletableFuture.runAsync(() -> {
			try {
				Thread.sleep(delayMs);
			} catch (InterruptedException ex) {
				throw new CompletionException(ex);
			}
		});

		CompletableFuture<String> result2 = result1.thenApply((v) -> {
			try {
				Map<String, Object> map = new HashMap<>();
				map.put("requestId", requestId);
				map.put("userId", userId);
				map.put("delayMs", delayMs);
				/*
				int count = COUNTER.incrementAndGet();
				if (count % 100 == 0) {
					System.out.println(count);
				}
				map.put("counter", count);
				*/
				String json = mapper.writeValueAsString(map);
				return json;
			} catch (Exception ex) {
				throw new CompletionException(ex);
			}
		});
		CompletableFuture<ResponseEntity<String>> result3 = result2.thenApply((body) -> getResponseEntity(body));

		DeferredResult<ResponseEntity<String>> result = convert(result3);
		return result;

	}

	@RequestMapping("/not-async")
	public ResponseEntity<String> getResponse(
			@RequestParam("delayMs") long delayMs,
			@RequestParam("requestId") int requestId, 
			@RequestParam("userId") int userId) throws Exception {

		Thread.sleep(delayMs);
		Map<String, Object> map = new HashMap<>();
		map.put("requestId", requestId);
		map.put("userId", userId);
		map.put("delayMs", delayMs);
		/*
		int count = COUNTER.incrementAndGet();
		if (count % 100 == 0) {
			System.out.println(count);
		}
		map.put("counter", count);
		*/
		String json = mapper.writeValueAsString(map);
		return getResponseEntity(json);
	}

	public <T> DeferredResult<T> convert(CompletableFuture<T> cf) {
		DeferredResult<T> deferred = new DeferredResult<>();
		cf.whenComplete((result, error) -> {
			if (error != null) {
				deferred.setErrorResult(error);
			} else {
				deferred.setResult(result);
			}
		});
		return deferred;
	}

	private ResponseEntity<String> getResponseEntity(String body) {
		return new ResponseEntity<String>(body, HttpStatus.OK);
	}
}
