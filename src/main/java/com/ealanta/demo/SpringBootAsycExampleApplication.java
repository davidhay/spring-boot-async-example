package com.ealanta.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAsycExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAsycExampleApplication.class, args);
	}
}
